﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;

namespace App_for_Opii_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public class AnswerInlineQuery
    {
        public string InlineQueryId { get; set; }
        public List<InlineQueryResult> Results { get; set; }
        public int CacheTime { get; set; }
        public bool IsPersonal { get; set; }
        public string NextOffset { get; set; }
        
        int users_online = 0;
        int users_offline = 0;
        
    }
    public partial class MainWindow : Window
    {
        TelegramBotClient bot;
        Dictionary<long, Dictionary<long, Report>> reports = Properties.Settings.Default.Reports;
        Dictionary<long, TelegramTimer> timers = new Dictionary<long, TelegramTimer>();
        Dictionary<long, bool> reportNeed = Properties.Settings.Default.ReportNeed;

        const string instructionStr = "Инструкция\nВыберите интервал отправления сообщений, чтобы начать новую историю.";
        const string errorStr = "Ошибка, вы не найдены в списках пользователей. Попробуйте ввести /start и повторить.";
        static ReplyKeyboardMarkup mainKeyboard = new ReplyKeyboardMarkup(new Telegram.Bot.Types.KeyboardButton[][]
                {
                    new Telegram.Bot.Types.KeyboardButton[]{
                        new Telegram.Bot.Types.KeyboardButton("Интервал"),
                        new Telegram.Bot.Types.KeyboardButton("Отчеты")
                    },
                    new Telegram.Bot.Types.KeyboardButton[]
                    {
                        new Telegram.Bot.Types.KeyboardButton("Инструкция")
                    }
                }, true, false);

        static InlineKeyboardMarkup reportsKeyboard = new InlineKeyboardMarkup(new Telegram.Bot.Types.InlineKeyboardButton[][]
                {
                    new Telegram.Bot.Types.InlineKeyboardButton[]{
                        new Telegram.Bot.Types.InlineKeyboardButton("За день", "r 1"),
                        new Telegram.Bot.Types.InlineKeyboardButton("За неделю", "r 7"),
                        new Telegram.Bot.Types.InlineKeyboardButton("За месяц", "r 30"),
                    },
                    new Telegram.Bot.Types.InlineKeyboardButton[]
                    {
                        new Telegram.Bot.Types.InlineKeyboardButton("За все время", "r"),
                        new Telegram.Bot.Types.InlineKeyboardButton("Важное", "r important"),
                        new Telegram.Bot.Types.InlineKeyboardButton("Приятное", "r enjoyable"),
                    }
                });

        static InlineKeyboardMarkup laterAnswerKeyboard = new InlineKeyboardMarkup(new InlineKeyboardButton[] {
            new InlineKeyboardButton("Позже", "l")
        });

        enum TimeEnum { Later, Interval }
        static InlineKeyboardMarkup TimeKeyboard(TimeEnum e, bool sleep)
        {
            var t = "";
            if (e == TimeEnum.Later)
                t = "l";
            else if (e == TimeEnum.Interval)
                t = "t";
            if(!sleep)
                return new InlineKeyboardMarkup(new Telegram.Bot.Types.InlineKeyboardButton[][]
                    {
                        new Telegram.Bot.Types.InlineKeyboardButton[]{
                            new Telegram.Bot.Types.InlineKeyboardButton("15мин", t + " 15"),
                            new Telegram.Bot.Types.InlineKeyboardButton("30мин", t + " 30"),
                            new Telegram.Bot.Types.InlineKeyboardButton("60мин", t + " 60"),
                        },
                        new InlineKeyboardButton[]{
                            new Telegram.Bot.Types.InlineKeyboardButton("120мин", t + " 120"),
                            new Telegram.Bot.Types.InlineKeyboardButton("180мин", t + " 180"),
                            new Telegram.Bot.Types.InlineKeyboardButton("Сон", t + " sleep"),
                        }
                    });
            else
                return new InlineKeyboardMarkup(new Telegram.Bot.Types.InlineKeyboardButton[][]
                    {
                        new Telegram.Bot.Types.InlineKeyboardButton[]{
                            new Telegram.Bot.Types.InlineKeyboardButton("4ч", t + " 240"),
                            new Telegram.Bot.Types.InlineKeyboardButton("5ч", t + " 300"),
                            new Telegram.Bot.Types.InlineKeyboardButton("6ч", t + " 360"),
                        },
                        new InlineKeyboardButton[]{
                            new Telegram.Bot.Types.InlineKeyboardButton("7ч", t + " 420"),
                            new Telegram.Bot.Types.InlineKeyboardButton("8ч", t + " 480"),
                            new Telegram.Bot.Types.InlineKeyboardButton("Выкл", t + " off"),
                        }
                    });

        }

        static InlineKeyboardMarkup importantKeyboard = new InlineKeyboardMarkup(new Telegram.Bot.Types.InlineKeyboardButton[]
                {
                        new Telegram.Bot.Types.InlineKeyboardButton("Важное", "important"),
                        new Telegram.Bot.Types.InlineKeyboardButton("Приятное", "enjoyable"),
                });

        public MainWindow()
        {
            InitializeComponent();
            foreach(var d in Properties.Settings.Default.Timers)
            {
                var timer = new TelegramTimer(d.Value) { AutoReset = true };
                timer.Elapsed += Timer_Elapsed;
                timers.Add(d.Key, timer);
                timer.Start();
            }
            bot = new TelegramBotClient("373697105:AAEC2xD7ugLPKxYibWB3mYew-7ujUEqRfKg");
            bot.OnMessage += Bot_OnMessageAsync;
            bot.OnCallbackQuery += Bot_OnCallbackQuery;
            bot.StartReceiving();
        }

        private async void SendMessage(Message msg, long id, bool addDateToBody = false)
        {
            if (msg.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage)
            {
                if (addDateToBody)
                    bot.SendTextMessageAsync(id, msg.Date.ToString() + "\n\n" +  msg.Text).Wait();
                else
                    bot.SendTextMessageAsync(id, msg.Text).Wait();
            }
            else if (msg.Type == Telegram.Bot.Types.Enums.MessageType.PhotoMessage)
                bot.SendPhotoAsync(id, msg.Photo.Last().FileId).Wait();
        }

        private async void Bot_OnCallbackQuery(object sender, CallbackQueryEventArgs e)
        {
            var t = e.CallbackQuery.Data.Split(' ');
            if (e.CallbackQuery.Data == "important")
            {
                var rep = reports[e.CallbackQuery.Message.Chat.Id][e.CallbackQuery.Message.MessageId];
                rep.isImportant = !rep.isImportant;
                if (rep.isImportant)
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Сообщение помечено как важное.");
                }
                else
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Метка \"важное\" снята с сообщения.");
                }
            }
            else if (e.CallbackQuery.Data == "enjoyable")
            {
                var rep = reports[e.CallbackQuery.Message.Chat.Id][e.CallbackQuery.Message.MessageId];
                rep.isEnjoyable = !rep.isEnjoyable;
                if (rep.isEnjoyable)
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Сообщение помечено как приятное.");
                }
                else
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Метка \"приятное\" снята с сообщения.");
                }
            }
            else if (e.CallbackQuery.Data == "statistics")
            {
                var rep = reports[e.CallbackQuery.Message.Chat.Id][e.CallbackQuery.Message.MessageId];
                rep.isEnjoyable = !rep.isEnjoyable;
                if (rep.isEnjoyable)
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Статистика:");
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, Check_Statistics());
                }
                else
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Невозможно показать статистику");
                }
            }
            else if (t[0] == "t")
            {
                try
                {
                    if (t.Length != 1)
                    {
                        try
                        {
                            var time = Convert.ToInt32(t[1]) * 60000;
                            timers[e.CallbackQuery.Message.Chat.Id].Stop();
                            timers[e.CallbackQuery.Message.Chat.Id].ResetTime = time;
                            timers[e.CallbackQuery.Message.Chat.Id].Start();
                            await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Бот оповестит вас через " + t[1] + " минут");
                        }
                        catch (FormatException)
                        {
                            if (t[1] == "sleep")
                                await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Выберите интервал", replyMarkup: TimeKeyboard(TimeEnum.Interval, true));
                            else if (t[1] == "off")
                            {
                                await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Таймер отключен");
                                timers[e.CallbackQuery.Message.Chat.Id].Stop();
                            }
                        }
                    }

                }
                catch
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, errorStr);
                }
            }
            else if (t[0] == "l")
            {
                try
                {
                    if (t.Length != 1)
                    {

                        try
                        {
                            var time = Convert.ToInt32(t[1]) * 60000;
                            timers[e.CallbackQuery.Message.Chat.Id].Stop();
                            timers[e.CallbackQuery.Message.Chat.Id].Interval = time;
                            timers[e.CallbackQuery.Message.Chat.Id].Start();
                            await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Бот оповестит вас позже, через " + t[1] + " минут");
                        }
                        catch (FormatException)
                        {
                            if (t[1] == "sleep")
                                await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Выберите интервал", replyMarkup: TimeKeyboard(TimeEnum.Later, true));
                            else if (t[1] == "off")
                            {
                                await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, "Таймер отключен");
                                timers[e.CallbackQuery.Message.Chat.Id].Stop();
                            }
                        }
                    }
                    else
                    {
                        await bot.EditMessageReplyMarkupAsync(e.CallbackQuery.Message.Chat.Id, e.CallbackQuery.Message.MessageId, TimeKeyboard(TimeEnum.Later, false));
                    }
                }
                catch (Exception ee)
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, errorStr);
                }

            }
            else if (t[0] == "r")
            {
                try
                {

                    if (t.Length == 2 && t[1] == "important")
                    {
                        foreach (var r in reports[e.CallbackQuery.Message.Chat.Id])
                        {
                            if (r.Value.isImportant)
                                SendMessage(r.Value.Message, e.CallbackQuery.Message.Chat.Id, true);
                        }
                    }
                    else if (t.Length == 2 && t[1] == "enjoyable")
                    {
                        foreach (var r in reports[e.CallbackQuery.Message.Chat.Id])
                        {
                            if (r.Value.isEnjoyable)
                                SendMessage(r.Value.Message, e.CallbackQuery.Message.Chat.Id, true);
                        }
                    }
                    else
                    {
                        DateTime time = DateTime.MinValue;
                        if (t.Length == 2)
                        {
                            time = DateTime.UtcNow.AddDays(-Convert.ToInt32(t[1]));
                        }
                        foreach (var r in reports[e.CallbackQuery.Message.Chat.Id])
                        {
                            if (r.Value.Message.Date > time)
                                SendMessage(r.Value.Message, e.CallbackQuery.Message.Chat.Id, true);
                        }
                    }
                }
                catch
                {
                    await bot.SendTextMessageAsync(e.CallbackQuery.Message.Chat.Id, errorStr);
                }
            }
        }

        
        private async void Bot_OnMessageAsync(object sender, MessageEventArgs e)
        {
            string text = "Команда не распознана.";
            IReplyMarkup keyboard = mainKeyboard;
            var savereport = false;

            if (e.Message.Text == "/start" || e.Message.Text == "Инструкция")
            {
                users += 1;
                text = instructionStr;
                if (!reportNeed.ContainsKey(e.Message.Chat.Id))
                {
                    reportNeed.Add(e.Message.Chat.Id, false);
                }
                if (!reports.ContainsKey(e.Message.Chat.Id))
                {
                    reports.Add(e.Message.Chat.Id, new Dictionary<long, Report>());
                }
                if (!timers.ContainsKey(e.Message.Chat.Id))
                {
                    var timer = new TelegramTimer() { AutoReset = true };
                    timer.Data.UserId = e.Message.Chat.Id;
                    timer.Elapsed += Timer_Elapsed;
                    timers.Add(e.Message.Chat.Id, timer);
                }
            }
            else if (e.Message.Text == "/stat")
            {
                text = "Всего подключено к боту: " + reports.Count.ToString();
            }
            else if (e.Message.Text == "Интервал")
            {
                text = "Задайте периодичность отчетов.";
                keyboard = TimeKeyboard(TimeEnum.Interval, false);
            }
            else if (e.Message.Text == "Отчеты")
            {
                keyboard = reportsKeyboard;
                text = "Выберите интервал.";
            }
            else
            {
                try
                {
                    if (reportNeed[e.Message.Chat.Id])
                    {
                        text = "Отчет принят";
                        keyboard = importantKeyboard;
                        savereport = true;
                        reportNeed[e.Message.Chat.Id] = false;
                    }
                }
                catch
                {
                    text = errorStr;
                }
            }

            var msg = await bot.SendTextMessageAsync(e.Message.Chat.Id, text, replyMarkup: keyboard);
            if (savereport)
            {
                try
                {
                    reports[e.Message.Chat.Id].Add(msg.MessageId, new Report(e.Message));
                }
                catch
                {
                    await bot.SendTextMessageAsync(e.Message.Chat.Id, errorStr);
                }
            }
        }

        private async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var t = sender as TelegramTimer;
            await bot.SendTextMessageAsync(t.Data.UserId, DateTime.UtcNow.ToString() +  "\n\n1. Отчет/что сделал (текст, фото)\n2. Комментарий\n 3. Цель на следующие " + (t.ResetTime / 60000).ToString() + "мин.", replyMarkup: laterAnswerKeyboard);
            reportNeed[t.Data.UserId] = true;
            t.Interval = t.ResetTime;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            bot.StopReceiving();
            Properties.Settings.Default.Timers.Clear();
            foreach (var d in timers)
            {
                Properties.Settings.Default.Timers.Add(d.Key, d.Value.Data);
            }
            Properties.Settings.Default.Save();
            users_online -= 1;
        }
        
        private async void Check_Statistics()
        {
            users_offline = all_users - users_offline;
            stata = "Пользователи онлайн: " + users_online.ToString() + "\n" + 
                    "Пользователи оффлайн: " + users_offline.ToString() ;
            return stata;
        }
        
    }
    
    
}
