﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace App_for_Opii_WPF
{
    public class TelegramTimer : System.Timers.Timer
    {
        [Serializable]
        public class TelegramTimerData
        {
            public long UserId { get; set; }
            public long resetTime;
        }
        public TelegramTimer() { }
        public TelegramTimer(TelegramTimerData data) { Data = data; Interval = data.resetTime; }
        public TelegramTimerData Data { get; private set; } = new TelegramTimerData();
        public long ResetTime { get { return Data.resetTime; } set { Data.resetTime = value; Interval = value; } }
    }
}
