﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace App_for_Opii_WPF
{
    public class Report
    {
        public Message Message { get; set; }
        public bool isImportant = false;
        public bool isEnjoyable = false;
        public Report() { }
        public Report(Message msg) { Message = msg; }
    }
}
